include:
  - project: computing/gitlab-ci-templates
    file:
      - debian.yml
      - python.yml
      - rhel.yml

stages:
  - dist
  - source
  - build
  - test
  - integration
  - docs
  - lint

# -- macros

.el8:
  image: igwn/base:el8-testing
  variables:
    EPEL: "true"

.buster:
  image: igwn/base:buster

.bullseye:
  image: igwn/base:bullseye

# -- dist -------------------
#
# This job makes the ligo-gracedb-X.Y.Z.tar.gz
# distribution and uploads it as a job
# artifact
#

tarball:
  extends:
    # https://computing.docs.ligo.org/gitlab-ci-templates/python/#.python:build
    - .python:build
  image: python:3
  stage: dist

# -- source packages --------
#
# These jobs make src RPMs
#

.source:
  stage: source
  needs:
    - tarball
  variables:
    TARBALL: "ligo-gracedb-*.tar.*"

.srpm:
  extends:
    # https://computing.docs.ligo.org/gitlab-ci-templates/rhel/#.rhel:srpm
    - .rhel:srpm
    - .source

srpm:el8:
  extends:
    - .srpm
    - .el8

.dsc:
  extends:
    # https://computing.docs.ligo.org/gitlab-ci-templates/debian/#.debian:dsc
    - .debian:dsc
    - .source
  before_script:
    - !reference [".debian:dsc", "before_script"]
    # unpack the tarball, update the debian changelog, and pack it up again
    - apt-get -yqq install devscripts
    - export DEBFULLNAME="GitLab"
    - export DEBEMAIL="gitlab@git.ligo.org"
    - tar -zxf ${TARBALL}
    - (cd ligo-gracedb-*/ && dch --rebuild 'ci')
    - tar -zcf ${TARBALL} ligo-gracedb-*/

dsc:buster:
  extends:
    - .dsc
    - .buster

dsc:bullseye:
  extends:
    - .dsc
    - .bullseye

# -- binary packages --------
#
# These jobs generate binary RPMs
# from the src RPMs
#

.binary:
  stage: build

.rpm:
  extends:
    # https://computing.docs.ligo.org/gitlab-ci-templates/rhel/#.rhel:rpm
    - .rhel:rpm
    - .binary
  variables:
    SRPM: "python-ligo-gracedb-*.src.rpm"

rpm:el8:
  extends:
    - .rpm
    - .el8
  needs:
    - srpm:el8

.deb:
  extends:
    # https://computing.docs.ligo.org/gitlab-ci-templates/debian/#.debian:deb
    - .debian:deb
    - .binary
  variables:
    DSC: "ligo-gracedb_*.dsc"

deb:buster:
  extends:
    - .deb
    - .buster

deb:bullseye:
  extends:
    - .deb
    - .bullseye

# -- test -------------------
#
# These jobs run the tests on
# all supported platforms
#

# -- test with pip

.test:
  extends:
    # https://computing.docs.ligo.org/gitlab-ci-templates/python/#.python:pytest
    - .python:pytest
  stage: test
  needs: []
  image: python
  variables:
    PYTHON: "python3"
  script:
    - python3 setup.py test --addopts "--cov ligo.gracedb --junitxml=junit.xml"

test:python3.8:
  extends:
    - .test
  image: python:3.8

test:python3.9:
  extends:
    - .test
  image: python:3.9

test:python3.10:
  extends:
    - .test
  image: python:3.10

test:python3.11:
  extends:
    - .test
  image: python:3.11

# -- test RHEL

.test:el:
  extends:
    - .test
  before_script:
    # set up yum caching
    - !reference [".rhel:base", before_script]
    # configure EPEL
    - yum -y -q install epel-release && yum -y -q install epel-rpm-macros
    # install testing dependencies
    - PY3=$(rpm --eval '%{?python3_pkgversion:%{python3_pkgversion}}%{!?python3_pkgversion:3}')
    - yum -y -q install
          findutils
          python${PY3}-coverage
          python${PY3}-pytest
          python${PY3}-pytest-cov
          python${PY3}-pytest-runner
    # install our package(s)
    - yum -y -q install ligo-gracedb-*.rpm python3*-ligo-gracedb-*.rpm

test:el8:
  extends:
    - .test:el
    - .el8
  needs:
    - rpm:el8

# -- test Debian

.test:debian:
  extends:
    - .test
    - .debian:base
  before_script:
    # set up apt
    - !reference [".debian:base", before_script]
    # setup local apt repository
    - apt-get -y -q -q install local-apt-repository
    - mkdir -pv /srv/local-apt-repository
    # fill our local apt repo and rebuild it
    - mv -v *.deb /srv/local-apt-repository
    - /usr/lib/local-apt-repository/rebuild
    - apt-get -y -q update
    # install our package(s)
    - apt-get -y install
          ligo-gracedb
          python3-ligo-gracedb
    # install testing dependencies
    - apt-get -y -q install
          findutils
          python3-coverage
          python3-pip
          python3-pytest
          python3-pytest-cov
          python3-pytest-runner

test:buster:
  extends:
    - .test:debian
    - .buster
  needs:
    - deb:buster

test:bullseye:
  extends:
    - .test:debian
    - .bullseye
  needs:
    - deb:bullseye
  # max pin on python3-cryptography is too restrictive
  # for bullseye
  allow_failure: true

# -- integration ------------

.integration:
  extends:
    - .python:test
  stage: integration
  needs: []
  image: python
  before_script:
    # source: https://wiki.ligo.org/AuthProject/LIGOCILOGONRobotCertificate
    - echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections
    - apt-get update && apt-get install -y curl krb5-user xsltproc
    - echo "${ROBOT_KEYTAB}" | base64 -d | install -m 0600 /dev/stdin keytab
    - ROBOT_PRINCIPAL=$(klist -k keytab | head -n 4 | tail -n 1 | sed -E 's/^.* +//')
    - kinit $ROBOT_PRINCIPAL -k -t keytab
    - ./scripts/ligo-proxy-init -k
  script:
    - python setup.py test --addopts "-m integration --cov ligo.gracedb"
  # only run on the lscsoft repo that has the credentials
  only:
    - branches@computing/gracedb/client
  # Do not run jobs simultaneously to avoid concurrent superevent 
  # creation. Staggering with when/start_in worked marginally. This
  # was inspired by:
  # https://about.gitlab.com/blog/2020/01/21/introducing-resource-groups/
  # and gracedb_sdk's pipeline.
  resource_group: integration-test

integration:python3.8:
  extends:
    - .integration
  image: python:3.8

integration:python3.9:
  extends:
    - .integration
  image: python:3.9

integration:python3.10:
  extends:
    - .integration
  image: python:3.10

integration:python3.11:
  extends:
    - .integration
  image: python:3.11

# -- docs -------------------
# Although we don't host the docs on Gitlab Pages,
# we still want to check that they build properly.
# ---------------------------

docs:
  extends:
    # https://computing.docs.ligo.org/gitlab-ci-templates/python/#.python:sphinx
    - .python:sphinx
  stage: docs
  needs: []
  image: python:3.7-slim
  variables:
    REQUIREMENTS: "-r requirements-doc.txt"
  script:
    - python -m sphinx -b html docs/source html

# -- linting ----------------

.lint:
  stage: lint

syntax:
  extends:
    # https://computing.docs.ligo.org/gitlab-ci-templates/python/#.python:flake8
    - .python:flake8
    - .lint
  needs: []
  variables:
    # don't fail the pipeline because of linting issues,
    # these are presented in the code-quality box in the
    # merge_request UI
    FLAKE8_OPTIONS: "--exit-zero"
    FLAKE8_TARGET: "*.py ligo/"

.rpmlint:
  extends:
    # https://computing.docs.ligo.org/gitlab-ci-templates/rhel/#.rhel:lint
    - .rhel:lint
    - .lint
  variables:
    GIT_STRATEGY: fetch
    RPMLINT_OPTIONS: '--info --file .rpmlintrc'

rpmlint:el8:
  extends:
    - .rpmlint
    - .el8
  needs:
    - rpm:el8

.lintian:
  extends:
    # https://computing.docs.ligo.org/gitlab-ci-templates/debian/#.debian:lint
    - .debian:lint
    - .lint
  variables:
    LINTIAN_OPTIONS: "--color always --suppress-tags new-package-should-close-itp-bug --allow-root --pedantic"

lintian:buster:
  extends:
    - .lintian
    - .buster
  needs:
    - deb:buster

lintian:bullseye:
  extends:
    - .lintian
    - .bullseye
  needs:
    - deb:bullseye
  variables:
    LINTIAN_OPTIONS: "--color always --suppress-tags initial-upload-closes-no-bugs,groff-message --allow-root --pedantic"
