Command-line interface
======================
A command-line tool (``gracedb``) is provided which reproduces some of the functionality of the Python module.
This tool is most useful for small-scale manual work and it is not recommended to be used in a programmatic fashion.

This section will provide some information about what functionality is available, but detailed information about the available commands and their arguments is provided in the internal documentation.
This documentation can be accessed by:

- ``gracedb --help``
- ``gracedb <command> --help``
- ``gracedb <command> <subcommand> --help``


Credentials
-----------
The command-line tool will look up credentials in some expected locations by default, just as the client class :py:class:`ligo.gracedb.rest.GraceDb` does.
You can also provide credentials to the command-line interface by using the ``--creds`` argument or the ``--username`` and ``--password`` arguments, or by setting environment variables as detailed in the reference for :py:class:`ligo.gracedb.rest.GraceDb`.


Events
------
- Create an event: ``gracedb create event``
- Search for events: ``gracedb search events``
- Retrieve an individual event: ``gracedb get event``
- Update an event: ``gracedb update event``


Superevents
-----------
- Create a superevent: ``gracedb create superevent``
- Search for superevents: ``gracedb search superevents``
- Retrieve an individual superevent: ``gracedb get superevent``
- Update a superevent: ``gracedb update superevent``
- Confirm a superevent as GW: ``gracedb confirm_as_gw``
- Expose a superevent to the public: ``gracedb expose``
- Make a superevent internal-only: ``gracedb hide``
- Add an event to a superevent: ``gracedb add event``
- Remove an event from a superevent: ``gracedb remove event``
- Get allowed superevent categories: ``gracedb info superevent_categories``


Annotations
-----------

Log entries
~~~~~~~~~~~
- Create a log entry (with optional file upload): ``gracedb create log``
- List all log entries for an event or superevent: ``gracedb list logs``
- Get a specific log entry: ``gracedb get log``

Files
~~~~~
- Upload a file: ``gracedb create log``
- Get a list of files associated with an event or superevent: ``gracedb list files``
- Download a file: ``gracedb get file``

VOEvents
~~~~~~~~
- Create a VOEvent: ``gracedb create voevent``
- List VOEvents for an event or superevent: ``gracedb list voevents``
- Get a specific VOEvent: ``gracedb get voevent``
- Get allowed VOEvent types: ``gracedb info voevent_types``

EM observations
~~~~~~~~~~~~~~~
- Upload EM observation data: ``gracedb create emobservation``
- List all EM observations for an event or superevent: ``gracedb list emobservations``
- Get a specific EM observation: ``gracedb get emobservation``
- Get the names EM groups for which data can be uploaded: ``gracedb info emgroups``

Signoffs
~~~~~~~~
- Create a superevent signoff: ``gracedb create signoff``
- Update a superevent signoff: ``gracedb update signoff``
- Delete a superevent signoff: ``gracedb delete signoff``
- List all signoffs for a superevent: ``gracedb list signoffs``
- Get a specific signoff: ``gracedb get signoff``
- Get a list of allowed signoff types: ``gracedb info signoff_types``
- Get a list of allowed signoff statuses: ``gracedb info signoff_statuses``

Labels
~~~~~~
- List which labels are available on the server: ``gracedb info labels``
- Add a label to an event or superevent: ``gracedb add label``
- Remove a label from an event or superevent: ``gracedb remove label``
- Get a list of labels associated with an event or superevent: ``gracedb list labels``
- Get a specific label instance: ``gracedb get label``

Tags
~~~~
- Add a tag to a log entry: ``gracedb add tag``
- Remove a tag from a log entry: ``gracedb remove tag``


Other
-----
- Check your connection to the server: ``gracedb ping``
- Show the credentials that the client is using: ``gracedb credentials client``
- Show the user account the server has connected your credentials with: ``gracedb credentials server``
- Get allowed analysis groups: ``gracedb info groups``
- Get allowed analysis pipelines: ``gracedb info pipelines``
- Get allowed search types: ``gracedb info searches``
- Get allowed instruments: ``gracedb info instruments``
- Get code version being run by the server: ``gracedb info server_version``
