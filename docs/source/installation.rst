Installation
============
This section covers all of the currently available options for installing this package.


pip
---
.. code-block:: bash

    pip install ligo-gracedb


Conda
-----
See the instructions `here <https://anaconda.org/conda-forge/ligo-gracedb>`__ to install ligo-gracedb via conda.


Debian
------
Follow the instructions `here <https://computing.docs.ligo.org/guide/software/debian/>`__ to configure the IGWN Debian repository.

After the repository is configured, you can install ligo-gracedb by doing::

    apt-get update
    apt-get install python3-ligo-gracedb


Scientific Linux 7
------------------
Follow the instructions `here <https://computing.docs.ligo.org/guide/software/sl7/>`__ to configure the LSCSoft SL7 repository.
After the repository is configured, you can install ligo-gracedb by doing::

    yum update
    yum install python36-ligo-gracedb
